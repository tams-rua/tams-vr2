var Airtable = require('airtable');
Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: 'key70l2I9iE1rMpX6'
});
var base = Airtable.base('appzwNchEdGvMM1Fx');

base('Veille').select({
    // Selecting the first 3 records in Tableau:
    view: "Tableau"
}).eachPage(function page(records, fetchNextPage) {
    // This function (`page`) will get called for each page of records.

    records.forEach(function (record) {
        console.log('Retrieved', record.get('Synthèse'));
        //document.getElementById('addVeille').innerHTML = document.getElementById('addVeille').innerHTML +  record.get('Synthèse');
        document.getElementById('list-veille').insertAdjacentHTML('afterbegin', ' <div id="veille" class="card mb-3"> <div class="row g-0"> <div class="col-md-4"><img src="' + record.get('Image')[0].url +'" alt="IMAGE">' + record.get('Synthèse') + '</div><div class="col-md-8"> <div class="card-body"> <p class="card-text"><small class="text-muted">' + record.get('Date') + '</small></p><h5 class="card-title">' + record.get('Sujet') + '</h5> <p class="card-text">nt is a little bit longer.</p><p class="card-text"><small class="text-muted">' + record.get('Commentaire') + '</small></p><p class="card-text"><small class="text-muted">' + record.get('Liens') + '</small></p></div></div></div></div>');
    });

    // To fetch the next page of records, call `fetchNextPage`.
    // If there are more records, `page` will get called again.
    // If there are no more records, `done` will get called.
    fetchNextPage();

}, function done(err) {
    if (err) {
        console.error(err);
        return;
    }
});

function showV(){
    document.getElementById('createV').style.display='block';
    document.getElementById('addV').style.display='none';
    document.getElementById('feelV').style.display='block';
}

function maskV(){
    document.getElementById('createV').style.display='none';
    document.getElementById('addV').style.display='block';
    document.getElementById('feelV').style.display='none';
}